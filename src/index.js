import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';
import './styles/index.less';
import { StoreProvider, createStore } from 'easy-peasy';

import model from './store/model';
import App from './containers/App';

import * as serviceWorker from './serviceWorker';


/* Importing Google Web Font */
WebFont.load({
    google: {
        families: ['Lato:400,700,900', 'sans-serif']
    }
});

/* Creating Store */
const store = createStore(model);


ReactDOM.render(
    <StoreProvider store={store}>
        <App />
    </StoreProvider>,
    document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
