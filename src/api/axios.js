import axios from 'axios';


const instance = axios.create({
    baseURL: 'https://siemens-test-4b27e.firebaseio.com',
    timeout: 5000
});

export default instance;