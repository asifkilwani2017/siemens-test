import React from 'react'
import { Card, Button } from 'antd';
import {priceFormatter} from '../../utils/functions';

const { Meta } = Card;

export default function Todo({ car: { id, name, image, price }, disableSearch, searchFilters, selectCarDeal }) {

  const monthlyPayment = ( (price / searchFilters.months)).toFixed(2);
  const leaseMonths = searchFilters.months
  
  return (
    <Card
      hoverable
      cover={<img alt="example" src={image} />}
    >
      <Meta
        title={name}
        description={`Price: ${priceFormatter(price)}`}
      />
      <div className="other-info">
        <div className="monthly-amount-label">Monthly Payment</div>
        <div className="monthly-amount-text">{`${priceFormatter(monthlyPayment)}`}</div>
      </div>

      <div className="btn-row">
        <Button type="primary" size={"large"} disabled={disableSearch} onClick={() => selectCarDeal({id,monthlyPayment,leaseMonths})} block>Select</Button>
      </div>
    </Card>
  )
}
