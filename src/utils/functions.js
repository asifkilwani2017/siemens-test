

const priceFormatter = (value) => {
    var parts = value.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".")+ " SEK";

}

export { priceFormatter }