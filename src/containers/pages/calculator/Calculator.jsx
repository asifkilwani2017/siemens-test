import React, { useState } from "react";
import { Drawer, Row, Col, Button, notification } from "antd";
import { useAction } from "easy-peasy";

import Filters from "./partials/Filters";
import FiltersResults from "./partials/FiltersResults";

export default function Calculator() {
  const [drawerVisible, showHideDrawer] = useState(false);
  const drawerHandler = e => showHideDrawer(!drawerVisible);

  // State: to hold selected car deal data.
  const [selectedCarDeal, setCarDealData] = useState("");

  // Pass on as props to filters results to fetch the selected car deal
  const showSelectedDeal = data => {
    setCarDealData(data); // update state
    drawerHandler()
  };

  // Action: Save deal in the firebase
  const saveCarDeal = useAction(actions => actions.order.confirmDeal);

  const confirmDealHandler = async () => {
    const response = await saveCarDeal(selectedCarDeal);
    let args;

    if (response.status === 200) {
      args = {
        message: "Deal Placed",
        description: "Your deal has placed successfully in our system"
      };
    } else {
      args = {
        message: "Error",
        description: "Error occurred while placing deal order"
      };
	}
	drawerHandler()
    openNotification(args);
  };

  const openNotification = args => {
    notification.config({
      placement: "topLeft",
      duration: 0
    });
    notification.open(args);
  };

  return (
    <>
      <div className="page--leasing-calculator">
        <header>
          <h1>Vehicle Leasing Calculator</h1>
        </header>

        <div className="container">
          <Row gutter={16} type="flex" justify="center">
            <Col xs={24} sm={24} md={24} lg={6} xl={6}>
              <div className="col-left">
                <Filters />
              </div>
            </Col>
            <Col xs={24} sm={24} md={24} lg={16} xl={16}>
              <div className="col-right">
                <FiltersResults showSelectedDeal={showSelectedDeal} />
              </div>
            </Col>
          </Row>
        </div>
      </div>

      <Drawer
        title="Vehicle Deal"
        placement="right"
        width={340}
        closable={false}
        onClose={drawerHandler}
        visible={drawerVisible}
      >
        <img src={selectedCarDeal.image} alt="" />
        <h2>{selectedCarDeal.name}</h2>
        <p>Price: {selectedCarDeal.price}</p>
        <p>Monthly Payment: {selectedCarDeal.monthlyPayment}</p>
        <p>Total Months: {selectedCarDeal.leaseMonths}</p>
        <Button
          type="primary"
          size={"large"}
          style={{ marginTop: "50px" }}
          onClick={confirmDealHandler}
          block
        >
          Confirm Deal
        </Button>
        <Button
          size={"large"}
          onClick={drawerHandler}
          style={{ marginTop: "20px" }}
          block
        >
          Cancel
        </Button>
      </Drawer>
    </>
  );
}
