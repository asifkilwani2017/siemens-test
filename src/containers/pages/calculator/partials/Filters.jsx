import React, {useState} from 'react';
import { useAction, useStore } from 'easy-peasy';
import { Slider, Button } from 'antd';
import {priceFormatter} from '../../../../utils/functions';

export default function Filters() {

    // Fetch filters default values
    const filters = useStore(state => state.filters.items)

    // Check if search is enabled or disabled
    const disableSearch = useStore(state => state.main.disableSearch)

    
    // Set state to track price filter change
    const [price, setPrice] = useState([filters.price.min, filters.price.max]);
    const priceSliderChange = value => setPrice(value);

    // Set state to track months filter change
    const [months, setMonths] = useState(filters.months.max);
    const monthSliderChange = value => setMonths(value);    


    // Update store with filter search query
    const updateSearch = useAction(actions => actions.search.update)

    const handleSearchClick = e => {
        const data = {
            price: {
                min: price[0],
                max: price[1]
            }, 
            months: months
        }

        updateSearch(data)
    }

    return (
        <>
            <h3 className="heading">Filters</h3>

            <div className="filter-row">
                <h4>Price</h4>
                <Slider 
                    range 
                    tooltipVisible 
                    disabled={disableSearch}
                    min={filters.price.min} 
                    max={filters.price.max} 
                    step={10000} 
                    tipFormatter={priceFormatter} 
                    defaultValue={[filters.price.min, filters.price.max]} 
                    onChange={priceSliderChange} 
                />
            </div>

            <div className="filter-row">
                <h4>Months</h4>
                <Slider 
                    tooltipVisible  
                    disabled={disableSearch}
                    min={filters.months.min} 
                    max={filters.months.max}  
                    step={2} 

                    defaultValue={filters.months.max} 
                    onChange={monthSliderChange} 
                />
            </div>

            <div className="filter-row no-margin">
                <Button type="primary" size={"large"} disabled={disableSearch} onClick={handleSearchClick} block>Show Results</Button>
            </div>

        </>
    );
}