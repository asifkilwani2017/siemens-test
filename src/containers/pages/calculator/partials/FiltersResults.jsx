import React from 'react';
import { useStore, useAction } from 'easy-peasy';
import {Row, Col } from 'antd';
import Car from '../../../../components/cards/Car';
import {priceFormatter} from '../../../../utils/functions';
import _ from 'lodash';

export default function FiltersResults({showSelectedDeal}) {

	// Fetch Filter/Search query from store
	const searchFilters = useStore(state => state.search.items)

	// Check if search is enabled or disabled
	const disableSearch = useStore(state => state.main.disableSearch)

	// Fetch Cars list data from store
	const cars = useStore(state => state.cars.items)
	const carsListArr = Object.values(cars)
	const orderedList = _.orderBy(carsListArr, ['price'], ['asc']);
	
	// Filter list by Min and Max Price
	const filteredList = orderedList.filter(car => {
		return Number(car.price) >= Number(searchFilters.price.min) && Number(car.price) <= Number(searchFilters.price.max)
	});

	// Action: Fetch selected car information from store
	const getSelectedCarData = useAction(actions => actions.cars.fetchById)

	// Fetch selected deal data and pass to parent component
	const selectCarDeal = async (value) => {
		const {id,monthlyPayment,leaseMonths} = value;
		const carData = await getSelectedCarData(id)
		var newState = {
			...carData,
			price: priceFormatter(carData["price"]),
			monthlyPayment:priceFormatter(monthlyPayment),
			leaseMonths: leaseMonths+" Months"
		}
		showSelectedDeal(newState)
	}

	const generateList = filteredList.map(car => (
		<Col key={car.id} xs={24} sm={24} md={24} lg={12} xl={8}>
			<Car car={car} searchFilters={searchFilters} disableSearch={disableSearch} selectCarDeal={selectCarDeal} />
		</Col>
	));

	return (
		
		<Row gutter={16}>
			{generateList}
		</Row>
	
	)

}