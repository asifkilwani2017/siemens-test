import React, { useEffect } from 'react';
import { useAction } from 'easy-peasy'
import Calculator from './pages/calculator/Calculator';

export default function App() {

  const initialise = useAction(actions => actions.initialise)
  
  useEffect(() => {
    initialise()
  })

  return (
    <>
      <Calculator />
    </>
  )

}