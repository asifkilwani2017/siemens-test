import { effect } from 'easy-peasy';
import axios from '../api/axios';
import _ from 'lodash';

export default {

  main:{
    disableSearch: false,
    disableSearchAction: (state,payload) => {
      if(payload === 200){
        state.disableSearch = true       
      }
    }
  },
  filters: {
    items:{
      price: {
        min: 100000,
        max: 600000
      },
      months: {
        min: 6,
        max: 48
      },
    },
    update: (state,payload) => {
      state.items = payload;
    }
  },
  search: {
    items:{
      price: {
        min: 100000,
        max: 600000
      },
      months: 48,
    }, 
    update: (state,payload) => {
      state.items = payload;
    }       
  },
  cars: {
    items: {},
    fetchById: effect(async (dispatch, payload, { getState }) => {
      const car = await _.find(getState().cars.items, { 'id': payload });
      return car
    }),
    fetchedAll: (state, payload) => {
      state.items = payload;
    },
    fetchAll: effect(async dispatch => {
      try {
        const response = await axios.get('/cars.json');
        dispatch.cars.fetchedAll(response.data)
      }catch (error) {
        alert(error); 
      }
    }),
  },
  order: {
    items:[],
    dealConfirmed: (state, payload) => {
      state.items = payload;
    },    
    confirmDeal: effect(async (dispatch, payload) => {
      try {
        const response = await axios.post('/orders.json', payload);
        dispatch.order.dealConfirmed(payload)
        await dispatch.main.disableSearchAction(response.status)
        return response
      }catch (error) {
        console.log(error)
        return error; 
      }
    }),    
  },
  initialise: effect(async dispatch => {
    await dispatch.cars.fetchAll()
  }),
}
